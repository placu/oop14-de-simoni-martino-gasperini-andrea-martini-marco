package file_manager;

import java.util.Map;

public interface LevelReader {

	/**
	 * @return La descrizione di ogni livello, presa dal file associata al LevelReader.
	 */
	
	public Map<String,Livello> leggiDatiLivelli();
	
}
