
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;


/*
 * Lo zombiPorta porta con se una enorme porta che lo difende dai colpi iniziali
 * fino a quando la vita della porta non scende a 0.
 * 
 *
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */


public class ZombiPorta extends Zombi {

	
	private static String img_filePath = "zombi_porta.jpeg";
	
	private double lifeOggetto = 30.0;
	
 public ZombiPorta(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_MEDIA,Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_MEDIO, TipoTerreno.CORTILE);
		
	}

 /*Override della funzione prendi danno perchè lo zombi infomato inizialmente il danno lo prende l'oggetto
  * e solo quando la vita dell'oggetto è uguale a 0 il danno lo prende lo zombie.
 */
	@Override
	public void prendiDanno(double danno) {
		
		while(this.lifeOggetto !=0){
			
		this.lifeOggetto -= danno;
		
		}
		
		super.prendiDanno(danno);
				
	}


	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller,Posizione2D posizione) {


		if(this.canAct(tempo_trascorso)){
			
		
		  Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento(new Posizione2D(-1,0),posizione,this, NessunAzione.getInstance() ),posizione);
			
			
			
			controller.insert(azioneDaImmettere);
		}		
	}
	
	
     public double getLifeOggetto(){
		
		return this.lifeOggetto;
	}

	
	
}
