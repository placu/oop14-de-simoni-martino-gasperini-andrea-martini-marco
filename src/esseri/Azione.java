package esseri;

/**
 * 
 * Le azioni sono lo strumento il modo tramite il quale gli enti del gioco effettivo comunicano con il controller.
 *
 *  @author Martino De Simoni
 */


public abstract class Azione {
	
	int animazione; // Metto un int, tanto le animazioni non le sappiamo fare
	
	public final Azione inCasoDiFallimento; //Se la prima azione fallisce, si esegue questo campo. 
	
	public Azione(Azione incasodifallimento){
		
		this.inCasoDiFallimento = incasodifallimento;
	}
	
	public Azione(){
		
		this.inCasoDiFallimento = NessunAzione.getInstance();
		
	}
	
}