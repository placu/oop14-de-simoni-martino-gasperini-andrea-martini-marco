package esseri;

/**
 * 
 * @author Martino De Simoni
 *
 * Azione di attacco comunicata dagli enti al controller.
 *
 */

public class Attacco extends Azione {
	
	public final TipoEssere chiAttaccare; //Piante? Zombie? Pallini? Uso le enum perchÄ� sono piÅ facili da usare. 
	public final Posizione2D distanzaDaAttaccante;
	public final Posizione2D posizioneAttaccante;
	public final Posizione2D doveAttaccare;
	
	public final Double danno; 
	
	public Attacco(TipoEssere _chiAttaccare,Posizione2D _distanzaDaAttaccante,double _danno,Azione inCasoDiFallimento,Posizione2D _posizioneAttaccante){
		super(inCasoDiFallimento);
	    this.chiAttaccare = _chiAttaccare;
		this.distanzaDaAttaccante = _distanzaDaAttaccante;
		this.danno = _danno;
		this.posizioneAttaccante = _posizioneAttaccante;
		
		this.doveAttaccare = posizioneAttaccante.sumPositions(distanzaDaAttaccante);
	}
	
}
