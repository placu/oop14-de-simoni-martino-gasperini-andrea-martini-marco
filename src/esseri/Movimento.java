package esseri;

/**
 * 
 * Classe per comunicare al controller un movimento dell'oggetto che comunica.
 *
 * @author Martino De Simoni
 *
 */

public class Movimento extends Azione{
	
	public final Posizione2D vettoreMovimento;
    public final Posizione2D posizioneDiChiSiMuove;
    public final Posizione2D doveSpostarsi;
    public final IEssere chiSiMuove;
    
    public Movimento(Posizione2D vettoreMovimento, Posizione2D posizioneDiChiSiMuove, IEssere _chiSiMuove, Azione incasodifallimento){
    	
    	super(incasodifallimento);
		
    	this.posizioneDiChiSiMuove = posizioneDiChiSiMuove;
		
		this.vettoreMovimento = vettoreMovimento;
		
		this.doveSpostarsi = vettoreMovimento.sumPositions(posizioneDiChiSiMuove);
		
		this.chiSiMuove = _chiSiMuove;
	}
	
}
