/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;


/**
 * @author Andrea
 *Mina fa parte delle piante di supporto. Quando � giocata sul terreno, e non viene attaccata non fa nulla,
 *ma nel momento in cui lo zombi attacca la Mina, questa fa danni elevati nell'area limitrofa come viene indicato
 *nel metodo faiRobe
 *
 */
public class Mina extends Support {
	
	
private static String img_filePath = "mina.jpe";
		
	
	public Mina(){
		 
		 super(img_filePath, 0.1, Utility.PIANTA_DANNO_ALTO, 
					0, TipoTerreno.CORTILE);
		 
	}
	
	public void inviaAzione(int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {
		
		
		
		if(canAct(tempo_trascorso)){
			
			 if(attaccato == true && hoattaccato == false){ 
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(0,0),danno,NessunAzione.getInstance(), posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(1,1),danno,NessunAzione.getInstance(),posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(0,1),danno,NessunAzione.getInstance(),posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(1,0),danno,NessunAzione.getInstance(),posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(-1,1),danno,NessunAzione.getInstance() ,posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(1,-1),danno,NessunAzione.getInstance(),posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(-1,0),danno,NessunAzione.getInstance(),posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(0,-1),danno,NessunAzione.getInstance(),posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,  new Posizione2D(-1,-1),danno,NessunAzione.getInstance(),posizione));
				
				hoattaccato = true;
			}
		
			else if(attaccato && hoattaccato){				
				controller.insert(new Attacco(TipoEssere.PIANTA, new Posizione2D(0,0),this.life, NessunAzione.getInstance() , posizione));
			}
		}
		}
	

@Override
public int getSoliRichiesti() {
	
	return 125;
}

@Override
public TipoTerreno getTerreno() {
	
	return TipoTerreno.CORTILE;
}

@Override
public void eseguireAllaMorte() {

}

@Override
public boolean isDead() {

	return hoattaccato;
}

}

